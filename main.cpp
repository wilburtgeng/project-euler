#include <iostream>
#include <stdlib.h>
#include "problems.h"

using namespace std;

int main(int argc, char** argv)
{
    int problemNum = (argc == 2)
        ? strtol(argv[1], nullptr, 10)
        : 0;

    while (problemNum == 0)
    {
        string value;
        cout << "Please provide a problem number." << endl;
        cin >> value;

        problemNum = strtol(value.c_str(), nullptr, 10);
    }

    switch(problemNum)
    {
        case 1: cout << problem1()  << endl;
        case 2: cout << problem2() << endl;
    }
}
