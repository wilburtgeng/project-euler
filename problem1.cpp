#include <vector>
#include <string>
#include "problems.h"

std::string problem1()
{
    int threshold  = 1000;
    std::vector<int> multiples {3, 5};

    int sum = 0;
    for (int i = 0; i < threshold; i++)
    {
        for (int j = 0; j < multiples.size(); j++)
        {
            if (i % multiples[j] == 0)
            {
                sum += i;
                break;
            }
        }
    }

    std::string output = "If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.\n"
        "Find the sum of all the multiples of 3 or 5 below 1000.";

    return output + "\n\n" +  std::to_string(sum);
}
